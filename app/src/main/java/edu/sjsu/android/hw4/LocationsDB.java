package edu.sjsu.android.hw4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;




import androidx.annotation.Nullable;

import java.lang.reflect.Field;

public class LocationsDB  extends SQLiteOpenHelper {

    static String  databaseName = "maplocationsDB";
    static String  tableName = "locations";

    public static String primaryKey = "pkey";
    public static String latitude = "lat" ;
    public static String longitude = "long";
    public static String zoomLevel = "zl";

    private SQLiteDatabase db;



    public LocationsDB(@Nullable Context context) {
        super(context, databaseName, null, 1);
        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String crTbl =    "create table " + tableName + " ( " +
                primaryKey + " integer primary key autoincrement , " +
                latitude + " double , " +
                longitude + " double , " +
                zoomLevel + " text " +
                " ) ";

        db.execSQL(crTbl);

    }


    public long insert(ContentValues contentValues){
        long rowID = db.insert(tableName, null, contentValues);
        return rowID;

    }

    public int deleteAll(){
        int cnt = db.delete(tableName, null , null);
        return cnt;
    }

    public Cursor getLocations(){
        return db.query(tableName, new String[] { primaryKey, latitude , longitude, zoomLevel } , null, null, null, null, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}